<?php

require 'vendor/autoload.php';
use \Slim\Middleware\HttpBasicAuthentication\PdoAuthenticator;
use \Slim\Middleware\JwtAuthentication\RequestPathRule;
use \Slim\Middleware\JwtAuthentication\RequestMethodRule;
use \Firebase\JWT\JWT;

//Secret Key (should not be store in the code)
$key = "mega_secret_key";

// Create database handler
$db = new PDO('sqlite:data/db.sqlite3');

// Create and configure Slim App
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

// Add middleware for security
$app->add(new \Slim\Middleware\HttpBasicAuthentication([
    "path" => "/token",
    "realm" => "Protected",
    "authenticator" => new PdoAuthenticator([
        "pdo" => $db,
        "table" => "users",
        "user" => "email",
        "hash" => "password"
    ])
]));
$app->add(new \Slim\Middleware\JwtAuthentication([
    "secret" => $key,
    "path" => "/",
    "rules" => [
        new RequestPathRule([
            "passthrough" => ["/token", "/signup", "/ping", "/install","newpwd"]
        ]),
        new \Slim\Middleware\JwtAuthentication\RequestMethodRule([
            "passthrough" => ["OPTIONS"]
        ])
    ]
]));

// API to create and init the database
$app->get('/install', function ($request, $response, $args) use ($db) {
    $res = $db->exec('CREATE TABLE IF NOT EXISTS users (
                        id INTEGER PRIMARY KEY,
                        email TEXT UNIQUE,
                        password TEXT);');
    return $response->write("installed :". print_r($db->errorInfo()));
});

$app->get('/ping', function ($request, $response, $args) use ($db, $key) {
    //return $response->write(date(DATE_RFC2822));
    return $response->write(print_r($_SERVER));
});

// login / sign up API
$app->get('/token', function ($request, $response, $args) use ($db, $key) {
    // get user id from mail
    $email = $request->getParam('email');
    $st = $db->prepare('SELECT id FROM users WHERE email=:email;');
    $st->bindParam(':email', $email);
    $st->execute();
    $firstRow = $st->fetch();
    $id = $firstRow['id'];

    // TODO manage case we not found user_id

    //create token
    $tokenPayload = array(
        "user_id" => $id,
    );
    $token = JWT::encode($tokenPayload, $key);


    return $response->write(json_encode(array("token"=>$token)));
});

$app->post('/signup', function ($request, $response, $args) use ($key){
    $email = $request->getParam("email");
    // TODO validate email
    // TODO check it does not alrealy exist

    // create token
    $tokenPayload = array(
        "email" => $email,
        "scope" => "new_account",
    );
    $token = JWT::encode($tokenPayload, $key);

    // create email
    $to = $email;
    $subject = "Welcome to sporg!";
    $protocol = "http" . ($_SERVER['HTTPS'] ? 's' : '');
    $host =  $_SERVER['HTTP_HOST'];
    $body = sprintf("click here to validate subscription : %s://%s#newpwd?token=%s",$protocol,$host,$token);

    //return $response->write($body);
    return $response->write(mail($to,$subject,  $body));
});

$app->post('/newpwd', function ($request, $response, $args) use ($db, $key){
    $password = $request->getParam("password");
    $decoded = $request->getAttribute("token");
    $email = $decoded->email;

    $st = $db->prepare('INSERT INTO users (email, password) VALUES (:email,:password);');
    $st->bindParam(':email', $email);
    $st->bindParam(':password', password_hash($password, PASSWORD_DEFAULT));
    $st->execute();

    // create token
    $tokenPayload = array(
            "scope" => "all",
    );
    $token = JWT::encode($tokenPayload, $key);

    return $response->write(json_encode(array("token"=>$token)));
});

// Run app
$app->run();
