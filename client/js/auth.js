// Create an observable Object
// see http://riotjs.com/api/observable/#constructor
var auth = riot.observable();

function setToken(token){
    if (!token){
        $.ajaxSettings.headers = {};
        delete sessionStorage.token;
    }else{
        sessionStorage.token = token;
        $.ajaxSettings.headers = {
            'Authorization': 'Bearer ' + token
        };
    }
}


// get credentials from local storage
if (sessionStorage.token){
    setToken(sessionStorage.token);
}

// Try to login with the username, password.
auth.login = function(username, password) {
    $.ajax({
        type:'GET',
        url:'api/token',
        username:username,
        password:password,
        success: function(data){
            var d = JSON.parse(data);
            setToken(d.token);
            auth.trigger('login');
        },
        error: function(xhr, type){
            setToken(undefined);
            auth.trigger('loginFailed');
        }
    });
};

auth.signup = function(email) {
    $.ajax({
        type:'POST',
        url:'api/signup',
        data:{"email":email},
        success: function(){
                auth.trigger('signupSucceed');
        },
        error: function(xhr, type){
                auth.trigger('signupFailed');
        }
    });
};

// Return true is we are currently logged.
auth.isLogged = function(){
    return  sessionStorage.token;
};

auth.changePassword = function (token, password){
    $.ajax({
        type:'POST',
        url:'api/newpwd',
        data:{"password":password},
        headers:{
            'Authorization': 'Bearer ' + token
        },
        success: function(data){
              var d = JSON.parse(data);
              setToken(d.token);
              auth.trigger('login');
        },
        error: function(xhr, type){
                auth.trigger('changePasswordFailed');
        }
    });
};

auth.logout = function(){
    setToken(null);
    auth.trigger('logout');
}
