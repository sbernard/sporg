<!-- login view -->
<newpwd>
    Type your new password :
    <form onsubmit="{ changePassword }">
        <input name="password1" type="password" placeholder="password1"></input>
        <input name="password2" type="password" placeholder="password2"></input>
        <button name="submit">log in</button>
    </form>
    <div>{changePasswordFailedMessage}</div>
    <script>
        var self = this;

        // Try to login
        changePassword() {
            auth.changePassword(riot.route.query().token,this.password1.value);
        };

        // If change password failed, display error message
        auth.on('changePasswordFailed', function() {
            self.changePasswordFailedMessage = "Password change Failed !";
            self.update();
        });
    </script>
</newpwd>
