<!-- login view -->
<login>
    <form onsubmit="{ login }">
        <input name="username" type="text" placeholder="username"></input>
        <input name="password" type="password" placeholder="password"></input>
        <button name="submit">log in</button>
    </form>
    <div>{loginFailedMessage}</div>
    <form onsubmit="{ signup }">
        <input name="email" type="text" placeholder="email"></input>
        <button name="submit">Sign up</button>
    </form>
    <div>{signupFailedMessage}</div>

    <script>
        var self = this;

        // Try to login
        login() {
            auth.login(this.username.value, this.password.value);
        };

        // Try to signup
        signup() {
            auth.signup(this.email.value);
        };

        // If login failed display error message
        auth.on('loginFailed', function() {
            self.loginFailedMessage = "Login Failed !";
            self.update();
        });

        // If signup failed display error message
        auth.on('signupFailed', function() {
            self.signupFailedMessage = "signup Failed !";
            self.update();
        });

        // signup pending
        auth.on('signupSucceed', function() {
            self.signupFailedMessage = "An email is sending to you to confirm subscription.";
            self.update();
        })
    </script>
</login>
