<!-- application view -->
<app>
    <!-- the current page -->
    <div id='page'></div>

    <script>
        var self = this
        self.mountedPage = null // The current mountedPage
        self.loadedPages = {} // Map to know if the page is already compile

        // Load a page
        load(pageName) {
            // Unmount the current page
            if (self.mountedPage) {
                self.mountedPage.unmount(true)
            }

            // Mount the new page
            if (!self.loadedPages[pageName]) {
                riot.compile('client/tags/pages/' + pageName + ".tag", function() {
                    self.loadedPages[pageName] = true
                    self.mountedPage = riot.mount('div#page',pageName)[0]
                })
            } else {
                self.mountedPage = riot.mount('div#page',pageName)[0]
            }
        }

        // Method called each time the route change
        // see http://riotjs.com/api/route/#setup-routing
        riot.route(function(pageName) {
            if (!auth.isLogged()){
                if (pageName == "login"|| pageName=="newpwd"){
                    self.load(pageName)
                    self.update()
                }else{
                    // if not logged, go to login
                    self.pageToGoAfterLogin = pageName
                    return riot.route('login')
                }
            }else{
                if (!pageName || pageName == "login" || pageName=="newpwd"  ){
                    // default page is home page
                    pageName = 'home'
                    riot.route(pageName)
                }else{
                    self.load(pageName)
                    self.update()
                }
            }
        })

        // Method called when we are logged
        auth.on('login', function() {
            riot.route(self.pageToGoAfterLogin||'')
        })

        auth.on('logout', function() {
            riot.route('')
        })


        // load components
        riot.compile("client/tags/component/menu.tag", function() {
                riot.mount('menu')
        })


        // Start listening the url changes and also exec routing on the current url
        riot.route.start(true)
    </script>
</app>
